package com.amaranthine.archerymod.util;

public interface IHasModel 
{
	public void registerModels();
}
