package com.amaranthine.archerymod.util;

public class Reference
{
	public static final String MOD_ID = "am";
	public static final String NAME = "Archery Mod";
	public static final String VERSION = "1.0.0";
	public static final String ACCEPTED_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY_CLASS = "com.amaranthine.archerymod.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "com.amaranthine.archerymod.proxy.CommonProxy";
}
