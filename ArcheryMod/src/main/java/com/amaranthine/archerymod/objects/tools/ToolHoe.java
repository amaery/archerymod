package com.amaranthine.archerymod.objects.tools;

import com.amaranthine.archerymod.Main;
import com.amaranthine.archerymod.init.ModItems;
import com.amaranthine.archerymod.util.IHasModel;

import net.minecraft.item.ItemHoe;

public class ToolHoe extends ItemHoe implements IHasModel
{
	public ToolHoe(String name, ToolMaterial material)
	{
		super(material);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Main.archerytab);
		
		ModItems.ITEMS.add(this);
	}
	

	@Override
	public void registerModels() 
	{
		Main.proxy.registerItemRenderer(this,0,"inventory");
	}
	
}
