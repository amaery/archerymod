package com.amaranthine.archerymod.objects.items;

import com.amaranthine.archerymod.Main;
import com.amaranthine.archerymod.init.ModItems;
import com.amaranthine.archerymod.util.IHasModel;

import net.minecraft.item.Item;

public class ItemBase extends Item implements IHasModel
{
	public ItemBase(String name)
	{
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Main.archerytab);
		
		ModItems.ITEMS.add(this);
	}
	

	@Override
	public void registerModels() 
	{
		Main.proxy.registerItemRenderer(this,0,"inventory");
	}
	
}
