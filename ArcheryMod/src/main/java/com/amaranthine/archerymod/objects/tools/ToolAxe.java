package com.amaranthine.archerymod.objects.tools;

import com.amaranthine.archerymod.Main;
import com.amaranthine.archerymod.init.ModItems;
import com.amaranthine.archerymod.util.IHasModel;

import net.minecraft.item.ItemAxe;

public class ToolAxe extends ItemAxe implements IHasModel
{
	public ToolAxe(String name, ToolMaterial material)
	{
		super(material, 6.0F, -3.2F);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(Main.archerytab);
		
		ModItems.ITEMS.add(this);
	}

    @Override
	public void registerModels() 
	{
		Main.proxy.registerItemRenderer(this, 0, "inventory");
	}
	
}
