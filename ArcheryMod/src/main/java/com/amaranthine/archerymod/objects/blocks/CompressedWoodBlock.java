package com.amaranthine.archerymod.objects.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class CompressedWoodBlock extends BlockBase 
{

	public CompressedWoodBlock(String name, Material material) 
	{
		super(name, material);
		
		setSoundType(SoundType.WOOD);
		setHardness(3.0F);
		setResistance(90.0F);
		setHarvestLevel("axe", 0);
	}
	
}
