package com.amaranthine.archerymod.init;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {

	public static void init()
	{
		GameRegistry.addSmelting(ModBlocks.LOG_WRAPPED_BLOCK, new ItemStack(ModItems.INGOT_BASIC, 1), 0.5F);
	}
}
