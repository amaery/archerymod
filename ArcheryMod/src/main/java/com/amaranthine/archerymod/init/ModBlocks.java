package com.amaranthine.archerymod.init;
import java.util.ArrayList;
import java.util.List;

import com.amaranthine.archerymod.objects.blocks.BlockBase;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks 
{
	public static final List<Block> BLOCKS = new ArrayList<Block>();
	
	public static Block COMPRESSED_BASE_WOOD_BLOCK = new BlockBase("compressed_base_wood_block", Material.WOOD);
	public static Block LOG_WRAPPED_BLOCK = new BlockBase("log_wrapped_block", Material.WOOD);
}
