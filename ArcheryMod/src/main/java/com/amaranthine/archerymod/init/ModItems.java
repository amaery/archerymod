package com.amaranthine.archerymod.init;

import java.util.ArrayList;
import java.util.List;

import com.amaranthine.archerymod.objects.items.ItemBase;
import com.amaranthine.archerymod.objects.tools.ToolAxe;
import com.amaranthine.archerymod.objects.tools.ToolBow;
import com.amaranthine.archerymod.objects.tools.ToolHoe;
import com.amaranthine.archerymod.objects.tools.ToolPickaxe;
import com.amaranthine.archerymod.objects.tools.ToolSpade;
import com.amaranthine.archerymod.objects.tools.ToolSword;

import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraftforge.common.util.EnumHelper;

public class ModItems 
{
	
	public static final List<Item> ITEMS = new ArrayList<Item>();
	
	//Materials
	public static final ToolMaterial TOOL_BASIC_WOOD = EnumHelper.addToolMaterial("tool_basic_wood", 1, 1000, 4.0F, 3.0F, 25);
	
	//Items
	public static final Item STRING_COMPRESSED = new ItemBase("string_compressed");
	
	public static final Item BASE_FLETCHING = new ItemBase("base_fletching");
	public static final Item INGOT_BASIC = new ItemBase("ingot_basic");
	
	//Tools
	public static final ItemSword SWORD_BASIC = new ToolSword("sword_basic", TOOL_BASIC_WOOD);
	public static final ItemSpade SHOVEL_BASIC = new ToolSpade("shovel_basic", TOOL_BASIC_WOOD);
	public static final ItemPickaxe PICKAXE_BASIC = new ToolPickaxe("pickaxe_basic", TOOL_BASIC_WOOD);
	public static final ItemAxe AXE_BASIC = new ToolAxe("axe_basic", TOOL_BASIC_WOOD);
	public static final ItemHoe HOE_BASIC = new ToolHoe("hoe_basic", TOOL_BASIC_WOOD);
	public static final ItemBow BOW_BASIC = new ToolBow("bow_basic", TOOL_BASIC_WOOD);
	
}
