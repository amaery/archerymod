package com.amaranthine.archerymod;

import com.amaranthine.archerymod.init.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class ArcheryTab extends CreativeTabs
{
	public ArcheryTab(String label)
	{
		super("archerytab");
		this.setBackgroundImageName("archery.png");
	}
	
	@Override
	public boolean hasSearchBar() 
	{
		return false;
	}
	
	@Override
	public ItemStack getTabIconItem() 
	{
		return new ItemStack(ModItems.INGOT_BASIC);
	}	
}
